# PXE

Preboot Execution Environment. https://en.m.wikipedia.org/wiki/Preboot_Execution_Environment

# PXE at Wikipedia
* [*Preboot Execution Environment*
  ](https://en.m.wikipedia.org/wiki/Preboot_Execution_Environment)
* [*iPXE*](https://en.m.wikipedia.org/wiki/IPXE)

# Unofficial documentation
* [*Netboot a Fedora Live CD*
  ](https://fedoramagazine.org/netboot-a-fedora-live-cd/)
  2019-02 	Gregory Bartholomew
